/*
   UQAM / Département d'informatique
   Été 2013 
   TP2
     
   Nom   :  Selmi
   Prénom:  Seifeddine
   Code Permanent: SELS19018207
   
   Nom   :  Girard  
   Prénom:  Maxime
   Code Permanent: GIRM30058500
   
   Date de remise : 21 Juillet 2013
*/

require('coffee-script');

var express = require('express')
  , routes = require('./routes')
  , path = require('path')
  , http = require('http');

var app = express();

app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('stylus').middleware(__dirname + '/public'));
app.use(express.static(path.join(__dirname, 'public')));

if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index); 
app.get('/contact', routes.contactGetAll); 
app.get('/contact/:id', routes.contactGetId); 
app.post('/contact', routes.createContact);  
app.delete('/contact/:id', routes.contactDeleteId); 
app.put('/contact/:id',routes.modifyContact);  
app.get('/recherche/:terme', routes.recherche); 
app.post('/archiver/:id', routes.archiver); 

app.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
