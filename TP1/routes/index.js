/*
  INF4375 - Paradigmes des échanges internet.
  UQAM / Département d'informatique
  Été 2013
  TP1
  
  Nom   :  Selmi
  Prénom:  Seifeddine
  Code Permanent: SELS19018207
  
  Nom   :  Girard  
  Prénom:  Maxime
  Code Permanent: GIRM30058500
     
  Date de remise : 25 Juin 2013
*/

var DOMParser = require('xmldom').DOMParser;
var XMLWriter = require('xml-writer');
var iconv = require('iconv-lite');
var htmlParser = require("./htmlParser.js");
var fs = require("fs");
var http = require("http");

exports.index = function(req, res){
  res.render('index');
};

exports.info = function(req, res){
  
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With"); 
  
  var arrayHtmlResponse = [];
  var codeProg = req.params.codeProg;
  var sigle = req.params.sigle;
  var groupe = req.params.groupe; 
  var queryS = 'sigle='+sigle+'&code_prog='+codeProg+'&an_ses2=Automne+2013&Iframe=0';
  var post_opt = {
         hostname: 'www.websysinfo.uqam.ca',
         port: 80,
         path: '/regis/rwe_horaire_cours/' + queryS,
         method: 'POST',
         headers: {
           'Content-Type' : 'application/x-www-form-urlencoded ',
           'Content-Length' : queryS.length
       }
      }; //post options
  
var post_req = http.request(post_opt, function(result) {        
    result.on('data', function (chunk) {
        arrayHtmlResponse.push(iconv.decode(chunk,"ISO-8859-1")); 
      });   
    
    result.on('end', function(){
       var htmlParsed = htmlParser.parse(arrayHtmlResponse.join(" "), function(err, result){
           res.json(result);
          });
         });
       });
    post_req.write(queryS);  
    post_req.end();
 };

exports.writeXML = function(req,res){

  //res.header("Access-Control-Allow-Origin", "*");
  //res.header("Access-Control-Allow-Headers", "X-Requested-With"); 
     
 var codePermanent = req.params.codePermanent;
 var codePermanentComparison = "<codePermanent>"+codePermanent+"</codePermanent>";
 var sigleCours = req.params.sigleCours;
 var sigleGroupe = req.params.groupeCours;
 var isAlreadyWritten = fs.exists("./dataXML", function (alreadyWritten){
 
 if(alreadyWritten){
    
    var xmlFile = fs.readFileSync("./dataXML", {encoding: "utf8" }); 
    var doc = new DOMParser().parseFromString(fs.readFileSync("./dataXML", {encoding: "utf8" }));
    var lsCP = doc.getElementsByTagName("codePermanent"); 

    console.log(lsCP[0].toString()); 
   
    for(var i = 0; i < lsCP.length; i++){
        
       // if(lsCP.length == 0){ continue; }
        if(codePermanentComparison === lsCP[i].toString()){
        
           var aParentNode = doc.getElementsByTagName("codePermanent")[i].parentNode;
           var elementC = doc.createElement("cours");
           var elementG = doc.createElement("groupe");
           var elementS = doc.createElement("sigle");
           var textG = doc.createTextNode(sigleGroupe); elementG.appendChild(textG);
           var textS = doc.createTextNode(sigleCours); elementS.appendChild(textS);

           elementC.appendChild(elementG);
           elementC.appendChild(elementS);
           aParentNode.appendChild(elementC);
           fs.writeFile("./dataXML",doc.toString(),{"encoding":"utf8"},function(err){if(err){console.log("err")};})
           return true;
          } 
        }
            var liste = doc.getElementsByTagName("etudiants");
            var nbElement = liste.length;
	    console.log(nbElement);
            var elementE = doc.createElement("etudiant");
            var elementP = doc.createElement("codePermanent");
     	    var elementC = doc.createElement("cours");
     	    var elementG = doc.createElement("groupe");
     	    var elementS = doc.createElement("sigle");
            
            var textP = doc.createTextNode(codePermanent); elementP.appendChild(textP);
            var textG = doc.createTextNode(sigleGroupe); elementG.appendChild(textG);
            var textS = doc.createTextNode(sigleCours); elementS.appendChild(textS);
            
            elementE.appendChild(elementC);
            elementE.appendChild(elementP);
            elementC.appendChild(elementG);
            elementC.appendChild(elementS);
            liste[nbElement-1].appendChild(elementE);
            console.log(doc.toString());
           fs.writeFile("./dataXML",doc.toString(),{"encoding":"utf8"},function(err){if(err){console.log("err")};});
           return true;      
 } else {

    xw = new XMLWriter;
    xw.startDocument('1.0', 'UTF-8');
    xw.startElement('etudiants').startElement('etudiant',"").writeElement('codePermanent', codePermanent);
    xw.startElement('cours', "");
    xw.writeElement('sigle', sigleCours);
    xw.writeElement('groupe', sigleGroupe);
    xw.endDocument();
    fs.appendFile("./dataXML", xw.toString(), function(err){
      if(!err){
       console.log("SUCCESS : XML Written.");
       res.end("Fichier XML écrit.");      
      } else { res.end("Erreur, fichier XML non créer."); } 
    }); 
   }
  });
};
