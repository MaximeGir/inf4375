/*
   INF4375 - Paradigmes des échanges internet.
   UQAM / Département d'informatique
   Été 2013
   TP1

Nom   :  Selmi
Prénom:  Seifeddine
Code Permanent: SELS19018207

Nom   :  Girard
Prénom:  Maxime
Code Permanent: GIRM30058500

Date de remise : 25 Juin 2013
 */

$(document).ready(function() {

		var msgHeureux = "<p class='temp'><font class='temp' id='tempFont' color='blue'>";
		var temoin = false;
		var codeTemoin = false;

		var myArray = [];
		var tabSigle = [];

		var codeProgErrMsg = "<p class='temp'><font class='temp' id='tempFont' color='red'>Le code doit contenir 4 chiffres seulement.</font></p>";
		var codePermanentErrMsg = "<p class='temp'><font class='temp' id='tempFont' color='red'>Le sigle doit contenir 4 lettres alphabétique et 8 chiffres.</font></p>";
		var sigleErrMsg = "<p class='temp'><font class='temp' id='tempFont' color='red'>Le code doit contenir 3 lettres alphabétique et 4 chiffres.</font></p>";
		var groupeErrMsg = "<p class='temp'><font class='temp' id='tempFont' color='red'> Le groupe doit contenir deux chiffres seulement. </font></p>";
		var placeErrMsg = "<p class='temp'><font class='temp' id='tempFont' color='red'> Ce cours n'a plus de place disponible.</font></p> ";	

		$("#inscription").attr("disabled" , "disabled");

		$("#inscription").click(function(){

			var cPermanent = $("#codePermanent").val();

			if(myArray.length == tabSigle.length){
			for (var i = 0; i < myArray.length; i++) {  
			writeXML(cPermanent,tabSigle[i], myArray[i].groupe);
			}
			}
			});

		//Code pour valider le code de programme dans la "form" dynamiquement.

		$("#codeProgramme").keypress(function(){ 
				var codeProg =  $("#codeProgramme").val();
				var regExp =/^\d{3}$/;
				var carValide = (codeProg.length == 3);

				if((!carValide || (!regExp.test(codeProg))) && (temoin == false)){
				temoin = true;         
				$("#resultat").html(codeProgErrMsg);
				$("#green").remove();  
				} else if (carValide && (regExp.test(codeProg))){
				$(".temp").remove();
				$("#codeP").html("<img src='./images/greenCheck.png' id='green'/>");}
				temoin = false;}); 

		// Code pour valider code Permanent form dynamiquement 

		$("#codePermanent").keypress(function(){
				var codePermanent = $("#codePermanent").val();
				var nbCarCodePermanent = (codePermanent.length < 12);
				var regExpCodePermanent = /^([a-zA-Z]{4})([0-9]{7})$/;	 
				if((!nbCarCodePermanent) || (!regExpCodePermanent.test(codePermanent))){
				if(codeTemoin == false){
				codeTemoin = true;
				$("#resultat").html(codePermanentErrMsg); 
				$("#blue").remove();   
				}} if ((nbCarCodePermanent) && (regExpCodePermanent.test(codePermanent) == true)){
				$(".temp").remove();
				$("#codePerm").html("<img src='./images/greenCheck.png' id='blue'/>");} 
				codeTemoin = false;});

		//Code pour valider le sigle 1
		$("#sigle_1").keypress(function(){

				var sigle_1  = $("#sigle_1").val();
				var nbCarSigle = (sigle_1.length == 6);
				var regexSigle = /^([a-zA-Z]{3})([0-9]{3})$/;


				if((!nbCarSigle) || (!regexSigle.test(sigle_1))){
				if(codeTemoin == false){
				codeTemoin = true;
				$("#resultat").html(sigleErrMsg);
				}

				}      

				if ((nbCarSigle) && (regexSigle.test(sigle_1) == true)){
				$(".temp").remove();
				} 
				codeTemoin = false;
				});

		//Code pour valider le sigle 2
		$("#sigle_2").keypress(function(){

				var sigle_2  = $("#sigle_2").val();
				var nbCarSigle = (sigle_2.length == 6);
				var regexSigle = /^([a-zA-Z]{3})([0-9]{3})$/;


				if((!nbCarSigle) || (!regexSigle.test(sigle_2))){
				if(codeTemoin == false){
				codeTemoin = true;
				$("#resultat").html(sigleErrMsg);
				}

				}      

				if ((nbCarSigle) && (regexSigle.test(sigle_2) == true)){
				$(".temp").remove();
				} 
				codeTemoin = false;
				});

		//Code pour valider le sigle 3
		$("#sigle_3").keypress(function(){

				var sigle_3  = $("#sigle_3").val();
				var nbCarSigle = (sigle_3.length == 6);

				var regexSigle = /^([a-zA-Z]{3})([0-9]{3})$/;


				if((!nbCarSigle) || (!regexSigle.test(sigle_3))){
				if(codeTemoin == false){
				codeTemoin = true;
				$("#resultat").html(sigleErrMsg);
				}

				}      

				if ((nbCarSigle) && (regexSigle.test(sigle_3) == true)){
				$(".temp").remove();


				} 
				codeTemoin = false;
		});

		//Code pour valider le sigle 4
		$("#sigle_4").keypress(function(){

				var sigle_4  = $("#sigle_4").val();
				var nbCarSigle = (sigle_4.length == 6);

				var regexSigle = /^([a-zA-Z]{3})([0-9]{3})$/;


				if((!nbCarSigle) || (!regexSigle.test(sigle_4))){
				if(codeTemoin == false){
				codeTemoin = true;
				$("#resultat").html(sigleErrMsg);
				}

				}      

				if ((nbCarSigle) && (regexSigle.test(sigle_4) == true)){
				$(".temp").remove();


				} 
				codeTemoin = false;
		});

		//Code pour valider le sigle 5
		$("#sigle_5").keypress(function(){

				var sigle_5  = $("#sigle_5").val();
				var nbCarSigle = (sigle_5.length == 6);

				var regexSigle = /^([a-zA-Z]{3})([0-9]{3})$/;


				if((!nbCarSigle) || (!regexSigle.test(sigle_5))){
				if(codeTemoin == false){
				codeTemoin = true;
				$("#resultat").html(sigleErrMsg);
				}

				}      

				if ((nbCarSigle) && (regexSigle.test(sigle_5) == true)){
				$(".temp").remove();


				} 
				codeTemoin = false;
		});

		// Code pour valider les cours (in-form).

		var validateCourse = function(id){
			var sigleVal  = $(id).val();
			var nbCarSigle = (sigleVal.length == 6);
			var regexSigle = /^([a-zA-Z]{3})([0-9]{3})$/;

			if((!nbCarSigle) || (!regexSigle.test(sigleVal))){
				if(codeTemoin == false){
					codeTemoin = true;

					$("#resultat").html(sigleErrMsg);
				}

			}      

			if ((nbCarSigle) && (regexSigle.test(sigleVal) == true)){
				$(".temp").remove(); 
			} 
			codeTemoin = false; 
			return true;
		}


		//Code pour valider les groupes
		var validateGroup = function(id){
			id += "_groupe";
			var noGroup = $(id).val();
			var regex =/^\d{1}$/;
			if(!regex.test(noGroup)){

				$("#resultat").html(groupeErrMsg);   

				return false; 
			} else {
				$(".temp").remove(); 
				return true;
			}

		}


		var sendAjax = function(validationID){		
			if(validateCourse(validationID) && validateGroup(validationID)){		
				var idCopy = validationID;
				if(idCopy.charAt(0)=== "#") {
					idCopy = idCopy.slice(1);
				}
				var codeProg = $("#codeProgramme").val();
				var sigle = $(validationID).val();	 
				var groupeID = validationID + "_groupe";
				var classID = "."+idCopy;
				var groupe = $(groupeID).val();
				var url = "/info/"+ codeProg + "/" + sigle + "/" + groupe + "/";
				$.ajax({
type: "GET",
dataType: "json",
crossDomain: true,
url: url,

error: function() {
console.log("Err: formulaire incomplet"); 
},

success: function(response){
tabSigle.push(sigle);
console.log(response[0].places_restantes);
if(response[0].places_restantes > 0){
if(myArray.length !== 0){ 
var taille = myArray.length;					
verifConflitHoraire(response[0]);		  
if(taille < myArray.length){	
prepareInscription();
} else {
$("#resultat").html(" ");
$("#resultat").html("<strong>Il y a un conflit d'horraire avec ce cours </strong>");
}
} else {							
	myArray.push(response[0]);							
	console.log(myArray.length);
	prepareInscription();
}
} else {
	$("#resultat").html(" ");
	$("#resultat").html("<strong>Désolé il n'y a plus de place pour ce cours.</strong>");  
}
}
});


}else{ 
	return; 
}
}

var prepareInscription = function(){
	$("#inscription").removeAttr("disabled");
	$("#inscription").attr("enabled" , "enabled");
	$("#resultat").html(" ");
	$("#resultat").html("<strong>Vous pouvez vous inscrire! (appuyez sur le bouton 'INSCRIPTION)</strong>");
}

var writeXML = function(codePermanent, sigle, groupe){
	var xhr = $.ajax({
type: "GET",
dataType: "XML",
crossDomain: true,
url: "/xml/"+codePermanent+"/"+sigle+"/"+groupe,
error: function() {
console.log("Err: formulaire incomplet");   
},
success: function(dataXML){
console.log("Écriture XML.");
$("#resultat").html(" ");
$("#resultat").html("<strong>Vous etes inscrit !</strong>");
console.log(dataXML);
} 
});
$("#resultat").html(" ");
$("#resultat").html("<strong>Vous etes inscrit !</strong>");
}

function getSeconds(time){
	var ts = time.split(':');
	return Date.UTC(1970, 0, 1, ts[0], ts[1]) / 1000;
}

var verifConflitHoraire = function(data){
	var valide = true;
	for (var i = 0; i < myArray.length; i++) {
		if(data.seances[0].jour === myArray[i].seances[0].jour){ 
			if((getSeconds(data.seances[0].debut)) == getSeconds(myArray[i].seances[0].debut)) {
				console.log("Erreur: Conflit d'horaire !!!");
				valide = false;
			}else if(((getSeconds(data.seances[0].debut)) > getSeconds(myArray[i].seances[0].debut)) 
					&& ((getSeconds(data.seances[0].debut)) < getSeconds(myArray[i].seances[0].fin))){

				console.log("Erreur: Conflit d'horaire !!!");
				valide = false;

			}else if(((getSeconds(data.seances[0].fin)) > getSeconds(myArray[i].seances[0].debut))  
					&& ((getSeconds(data.seances[0].fin)) < getSeconds(myArray[i].seances[0].fin))){

				console.log("Erreur: Conflit d'horaire !!!");
				valide = false;

			}else if(((getSeconds(data.seances[0].debut)) < getSeconds(myArray[i].seances[0].debut))  
					&& ((getSeconds(data.seances[0].fin)) > getSeconds(myArray[i].seances[0].fin))){

				console.log("Erreur: Conflit d'horaire !!!");
				valide = false;

			}else if(((getSeconds(data.seances[0].debut)) > getSeconds(myArray[i].seances[0].debut))  
					&& ((getSeconds(data.seances[0].fin)) < getSeconds(myArray[i].seances[0].fin))){

				console.log("Erreur: Conflit d'horaire !!!");
				valide = false;
			}
		}
		if(myArray[i].seances.length > 1){

			if(data.seances[0].jour == myArray[i].seances[1].jour){

				if((getSeconds(data.seances[0].debut)) == getSeconds(myArray[i].seances[1].debut)) {
					console.log("Erreur: Conflit d'horaire !!!");
					valide = false;


				}else if(((getSeconds(data.seances[0].debut)) > getSeconds(myArray[i].seances[1].debut)) 
						&& ((getSeconds(data.seances[0].debut)) < getSeconds(myArray[i].seances[1].fin))){



					console.log("Erreur: Conflit d'horaire !!!");
					valide = false;


				}else if(((getSeconds(data.seances[0].fin)) > getSeconds(myArray[i].seances[1].debut))  
						&& ((getSeconds(data.seances[0].fin)) < getSeconds(myArray[i].seances[1].fin))){


					console.log("Erreur: Conflit d'horaire !!!");
					valide = false;



				}else if(((getSeconds(data.seances[0].debut)) < getSeconds(myArray[i].seances[1].debut))  
						&& ((getSeconds(data.seances[0].fin)) > getSeconds(myArray[i].seances[1].fin))){


					console.log("Erreur: Conflit d'horaire !!!");
					valide = false;

				}else if(((getSeconds(data.seances[0].debut)) > getSeconds(myArray[i].seances[1].debut))  
						&& ((getSeconds(data.seances[0].fin)) < getSeconds(myArray[i].seances[1].fin))){


					console.log("Erreur: Conflit d'horaire !!!");
					valide = false;
				}
			}//if
		}
		if(data.seances.length > 1){
			if(data.seances[1].jour == myArray[i].seances[0].jour){

				if((getSeconds(data.seances[1].debut)) == getSeconds(myArray[i].seances[0].debut)) {
					console.log("Erreur: Conflit d'horaire !!!");
					valide = false;


				}else if(((getSeconds(data.seances[1].debut)) > getSeconds(myArray[i].seances[0].debut)) 
						&& ((getSeconds(data.seances[1].debut)) < getSeconds(myArray[i].seances[0].fin))){



					console.log("Erreur: Conflit d'horaire !!!");
					valide = false;


				}else if(((getSeconds(data.seances[1].fin)) > getSeconds(myArray[i].seances[0].debut))  
						&& ((getSeconds(data.seances[1].fin)) < getSeconds(myArray[i].seances[0].fin))){


					console.log("Erreur: Conflit d'horaire !!!");
					valide = false;



				}else if(((getSeconds(data.seances[1].debut)) < getSeconds(myArray[i].seances[0].debut))  
						&& ((getSeconds(data.seances[1].fin)) > getSeconds(myArray[i].seances[0].fin))){


					console.log("Erreur: Conflit d'horaire !!!");
					valide = false;

				}else if(((getSeconds(data.seances[1].debut)) > getSeconds(myArray[i].seances[0].debut))  
						&& ((getSeconds(data.seances[1].fin)) < getSeconds(myArray[i].seances[0].fin))){


					console.log("Erreur: Conflit d'horaire !!!");
					valide = false;
				}
			}
		}
		if(data.seances.length > 1 && myArray[i].seances.length > 1){

			if(data.seances[1].jour == myArray[i].seances[1].jour){

				if((getSeconds(data.seances[1].debut)) == getSeconds(myArray[i].seances[1].debut)) {
					console.log("Erreur: Conflit d'horaire !!!");
					valide = false;

				}else if(((getSeconds(data.seances[1].debut)) > getSeconds(myArray[i].seances[1].debut)) 
						&& ((getSeconds(data.seances[1].debut)) < getSeconds(myArray[i].seances[1].fin))){

					console.log("Erreur: Conflit d'horaire !!!");
					valide = false;

				}else if(((getSeconds(data.seances[1].fin)) > getSeconds(myArray[i].seances[1].debut))  
						&& ((getSeconds(data.seances[1].fin)) < getSeconds(myArray[i].seances[1].fin))){

					console.log("Erreur: Conflit d'horaire !!!");
					valide = false;

				}else if(((getSeconds(data.seances[1].debut)) < getSeconds(myArray[i].seances[1].debut))  
						&& ((getSeconds(data.seances[1].fin)) > getSeconds(myArray[i].seances[1].fin))){

					console.log("Erreur: Conflit d'horaire !!!");
					valide = false;

				}else if(((getSeconds(data.seances[1].debut)) > getSeconds(myArray[i].seances[1].debut))  
						&& ((getSeconds(data.seances[1].fin)) < getSeconds(myArray[i].seances[1].fin))){

					console.log("Erreur: Conflit d'horaire !!!");
					valide = false;
				}
			}				
		}//endif	
	}// end for
	if(valide === true){
		myArray.push(data);	
		console.log("Cours est bien ajouté ");
	}	

	return myArray;
}// fin de la fonction

$("#sigle_1_groupe").keypress(function(){ sendAjax("#sigle_1"); });
$("#sigle_2_groupe").keypress(function(){ sendAjax("#sigle_2"); });
$("#sigle_3_groupe").keypress(function(){ sendAjax("#sigle_3"); });
$("#sigle_4_groupe").keypress(function(){ sendAjax("#sigle_4"); });
$("#sigle_5_groupe").keypress(function(){ sendAjax("#sigle_5"); });
});
